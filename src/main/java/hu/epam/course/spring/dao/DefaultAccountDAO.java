package hu.epam.course.spring.dao;

import hu.epam.course.spring.model.Account;

public class DefaultAccountDAO implements AccountDAO {

	private Account account;

	public DefaultAccountDAO(Account account) {
		this.account = account;
	}

	@Override
	public Account getExampleAccountDao() {
		return account;
	}

}
