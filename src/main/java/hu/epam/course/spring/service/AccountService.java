package hu.epam.course.spring.service;

import hu.epam.course.spring.model.Account;

public interface AccountService {
	Account getExampleAccount();
	
	boolean transferMoney(Account accountFrom, Account accountTo, double amount);
}
