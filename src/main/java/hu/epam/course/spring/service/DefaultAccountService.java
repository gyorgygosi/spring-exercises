package hu.epam.course.spring.service;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

import hu.epam.course.spring.dao.AccountDAO;
import hu.epam.course.spring.model.Account;

public class DefaultAccountService implements AccountService {

	private AccountDAO accountDAO;

	public DefaultAccountService(AccountDAO accountDAO) {
		this.accountDAO = accountDAO;
	}

	@Override
	public Account getExampleAccount() {
		return accountDAO.getExampleAccountDao();
	}

	@Override
	public boolean transferMoney(Account accountFrom, Account accountTo, double amount) {
		boolean result = false;
		if (accountFrom.getBalance() >= amount) {
			accountFrom.setBalance(accountFrom.getBalance() - amount);
			accountTo.setBalance(accountTo.getBalance() + amount);
			result = true;
		}
		return result;
	}

}
