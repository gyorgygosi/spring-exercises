package hu.epam.course.spring.methodinjection;

import org.springframework.beans.factory.annotation.Qualifier;


public class CommandManager {
	protected Command commandToExecute;

	public void process() {
		Command command = createCommand();

		command.execute();
		System.out.println("Command result: " + command.getResult());
	}

	public Command getCommandToExecute() {
		return commandToExecute;
	}

	public void setCommandToExecute(Command commandToExecute) {
		this.commandToExecute = commandToExecute;
	}
	
	protected Command createCommand() {
		return null;
	}

}
