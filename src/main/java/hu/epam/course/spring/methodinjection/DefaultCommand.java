package hu.epam.course.spring.methodinjection;

public class DefaultCommand implements Command {

	private static int totalInstanceCounter = 0;
	private int instanceCounter = 0;
	private boolean executed = true;
	
	public DefaultCommand() {
		instanceCounter = ++totalInstanceCounter;
	}
	
	@Override
	public void execute() {
		executed = true;
	}

	@Override
	public int getResult() {
		int ret = 0;
		if (executed) {
			ret = instanceCounter;
		}
		return ret;
	}

}
