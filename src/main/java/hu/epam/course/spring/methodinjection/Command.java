package hu.epam.course.spring.methodinjection;

public interface Command {
	void execute();

	int getResult();
}
