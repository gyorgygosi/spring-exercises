package hu.epam.course.spring.main;

import hu.epam.course.spring.model.Account;
import hu.epam.course.spring.model.User;
import hu.epam.course.spring.service.AccountService;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainApp {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("spring-config/application-context.xml");

		AccountService accountService = (AccountService) context.getBean("accountService");
		Account account = accountService.getExampleAccount();
		User user = account.getOwner();
		
		System.out.println(account);
		System.out.println(user);

		
		((ClassPathXmlApplicationContext) context).close();

	}

}
