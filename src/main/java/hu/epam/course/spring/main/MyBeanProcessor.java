package hu.epam.course.spring.main;

import hu.epam.course.spring.model.User;

import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class MyBeanProcessor implements BeanPostProcessor, BeanFactoryAware {

	private BeanFactory beanFactory;

	private List<Class<?>> observedClasses;

	public List<Class<?>> getObservedClasses() {
		return observedClasses;
	}

	public void setObservedClasses(List<Class<?>> observedClasses) {
		this.observedClasses = observedClasses;
	}

	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {

		for (Class<?> class1 : observedClasses) {
			if (class1.isAssignableFrom(bean.getClass())) {
				System.out.println("it is assignable");
			}
		}
		return bean;
	}

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		if (beanFactory.isTypeMatch(beanName, User.class)) {
			User myBean = (User) bean;
			System.out.println("_postProcess after. (user)");
			return myBean;
		} else {
			return bean;
		}
	}

	@Override
	public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
		this.beanFactory = beanFactory;
	}
}
