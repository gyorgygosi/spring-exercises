package hu.epam.course.spring.model;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.InitializingBean;

public class User implements BeanNameAware, InitializingBean {
	private String name;
	private int userId;
	private Account account;
	private Date birthDate;

	private Map<AddressType, List<Address>> addresses;

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Map<AddressType, List<Address>> getAddresses() {
		return addresses;
	}

	public void setAddresses(Map<AddressType, List<Address>> addresses) {
		this.addresses = addresses;
	}

	public static User createUser() {
		return new User();
	}

	@Override
	public String toString() {
		return "User [name=" + name + ", userId=" + userId + ", account=" + account + ", birthDate=" + birthDate
				+ ", addresses=" + addresses + "]";
	}

	@Override
	public void setBeanName(String name) {
		System.out.println("BeanNameAware: " + name);
	}

	@PostConstruct
	public void initPostConstruct() {
		System.out.println("@PostConstruct called");
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		System.out.println("afterPropertiesSet called");
	}

	// init-method
	public void init() {
		System.out.println("init-method called");
	}

}
